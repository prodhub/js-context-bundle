<?php

namespace ADW\JsContextBundle;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

/**
 * Class JsContext
 *
 * @package ADW\JsContextBundle
 * @author Artur Vesker
 */
class JsContext implements JsContextInterface
{

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $contexts = [];

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @inheritdoc
     */
    public function addData($name, $data, SerializationContext $context = null)
    {
        $this->data[$name] = $data;
        $this->contexts[$name] = $context;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function createView()
    {
        $normalizedData = [];

        foreach ($this->data as $name => $item) {
            if (is_scalar($item) || $item === null) {
                $normalizedData[$name] = $item;
            } else {
                $normalizedData[$name] = $this->serializer->toArray($item, $this->contexts[$name]);
            }
        }

        return [
            'data' => $normalizedData
        ];
    }

}