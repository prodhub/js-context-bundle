<?php

namespace ADW\JsContextBundle\EventListener;

use ADW\JsContextBundle\JsContextInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class CPAListener
 *
 * @package AppBundle\EventListener
 * @author Artur Vesker
 */
class CPAListener
{

    const COOKIE_NAME = '_cpa';

    /**
     * @var JsContextInterface
     */
    protected $jsContext;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @param JsContextInterface $jsContext
     * @param string $fieldName
     * @internal param array $config
     */
    public function __construct(JsContextInterface $jsContext, $fieldName = 'cpa')
    {
        $this->jsContext = $jsContext;
        $this->fieldName = $fieldName;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        try {
            $cpa = json_decode($request->cookies->get(self::COOKIE_NAME), true);
        } catch (\Exception $e) {
            $cpa = [];
        }

        if (!is_array($cpa)) {
            $cpa = [];
        }

        if ($source = $request->query->get('utm_source')) {
            $cpa[strtolower($source)] = $request->query->all();
        }

        $request->attributes->set(self::COOKIE_NAME, $cpa);
        $this->jsContext->addData($this->fieldName, $cpa);
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $cpa = $event->getRequest()->attributes->get(self::COOKIE_NAME);

        if (empty($cpa)) {
            return;
        }

        $event->getResponse()
            ->headers
            ->setCookie(new Cookie(self::COOKIE_NAME, json_encode($cpa), 0, '/', null, false));
    }

}