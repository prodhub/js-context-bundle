<?php

namespace ADW\JsContextBundle\EventListener;

use ADW\JsContextBundle\JsContextInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserListener
 *
 * @package ADW\JsContextBundle\EventListener
 * @author Artur Vesker
 */
class UserListener
{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var JsContextInterface
     */
    protected $jsContext;

    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $group;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param JsContextInterface $jsContext
     * @param string $fieldName
     * @param string $group
     */
    public function __construct(TokenStorageInterface $tokenStorage, JsContextInterface $jsContext, $fieldName, $group)
    {
        $this->tokenStorage = $tokenStorage;
        $this->jsContext = $jsContext;
        $this->fieldName = $fieldName;
        $this->group = $group;
    }

    /**
     * @param FilterControllerEvent $controllerEvent
     */
    public function onController(FilterControllerEvent $controllerEvent)
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        $this->jsContext->addData($this->fieldName, $token->getUser(), SerializationContext::create()->setGroups($this->group));
    }

}