<?php

namespace ADW\JsContextBundle;

use JMS\Serializer\SerializationContext;

/**
 * Interface JsContextInterface
 *
 * @package ADW\JsContextBundle
 */
interface JsContextInterface
{

    /**
     * @param $name
     * @param $data
     * @param SerializationContext $context
     * @return JsContextInterface
     */
    public function addData($name, $data, SerializationContext $context = null);

    /**
     * @return array
     */
    public function createView();

}