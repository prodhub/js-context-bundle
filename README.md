# Что такое? #
Вывод данных в контекст js.

# Установка #
В composer.json:

```
#!json
"repositories": [
   { "type": "vcs", "url": "git@bitbucket.org:prodhub/js-context-bundle.git" }
]
```
Выполнить:

```
#!bash
composer require adw/js-context-bundle

```

AppKernel.php:

```
#!php
$bundles = [
   new \ADW\JsContextBundle\ADWJsContextBundle()
];

```

В базовом шаблоне, где должен стать доступным контекст (например в head):

```
#!twig
{{ js_context() }}

```

# Использование #

```
#!php

$container->addData('my_awesome_simple_data', [
   'foo' => 'bar'
]);

$container->addData('my_awesome_serialized_data', $article);

$container->addData('my_awesome_serialized_data_with_group', $article, SerializationContext::create()->setGroups('short'));
```

# Стандартные listener'ы #

### User Listener ###
Добавление в контекст текущего юзера.
```
#!yaml
adw_js_context:
    user_listener:
        enabled: false #по умолчанию автодобавление юзера в контекст отключено
        field_name: user 
        group: Default #Группа сериализации для юзера
```
### CPA Listener ###
Добавление в контекст cpa (utm) данных, сохранение их в куках.
```
#!yaml
adw_js_context:
    cpa_listener:
        enabled: false #по умолчанию автодобавление cpa данных в контекст отключено
        field_name: cpa 
```

# Конфигурация (опционально) #
```
#!yaml

adw_js_context:
    var_name: test #название переменной контекста в js
    ...
```

Бандл включен в skeleton.