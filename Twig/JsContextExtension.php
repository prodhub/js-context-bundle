<?php

namespace ADW\JsContextBundle\Twig;

use Symfony\Component\HttpKernel\KernelInterface;
use ADW\JsContextBundle\JsContextInterface;

/**
 * Class JsContextExtension
 *
 * @package ADW\JsContextBundle\Twig
 * @author Artur Vesker
 */
class JsContextExtension extends \Twig_Extension
{

    /**
     * @var JsContextInterface
     */
    protected $jsContext;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var string
     */
    protected $varName;

    /**
     * @param KernelInterface $kernel
     * @param JsContextInterface $jsContext
     * @param $varName
     */
    public function __construct(KernelInterface $kernel, JsContextInterface $jsContext, $varName)
    {
        $this->jsContext = $jsContext;
        $this->kernel = $kernel;
        $this->varName = $varName;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('js_context', function() {
                $script  = '<script type="text/javascript"> var ' . $this->varName . ' = ' . json_encode($this->jsContext->createView()) . '</script>';

                if ($this->kernel->getEnvironment() == 'dev') {
                    $script .= '<script type="text/javascript">console.log(' . $this->varName . ');</script>';
                }

                return $script;
            }, ['is_safe' => ['html']])
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'js_context';
    }

}