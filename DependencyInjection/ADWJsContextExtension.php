<?php

namespace ADW\JsContextBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ADWJsContextExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config as $name => $value) {
            $container->setParameter('js_context.'.$name, $value);
        }

        //User Listener
        if ($config['user_listener']['enabled']) {
            $definition = new Definition('ADW\JsContextBundle\EventListener\UserListener', [
                new Reference('security.token_storage'),
                new Reference('js_context'),
                $config['user_listener']['field_name'],
                $config['user_listener']['group']
            ]);

            $definition->addTag('kernel.event_listener', ['event' => KernelEvents::CONTROLLER, 'method' => 'onController']);

            $container->setDefinition('js_context.user_listener', $definition);
        }

        //CPA Listener
        if ($config['cpa_listener']['enabled']) {
            $definition = new Definition('ADW\JsContextBundle\EventListener\CPAListener', [
                new Reference('js_context'),
                $config['cpa_listener']['field_name'],
            ]);

            $definition->addTag('kernel.event_listener', ['event' => KernelEvents::REQUEST, 'method' => 'onRequest']);
            $definition->addTag('kernel.event_listener', ['event' => KernelEvents::RESPONSE, 'method' => 'onResponse']);

            $container->setDefinition('js_context.cpa_listener', $definition);
        }


        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

}